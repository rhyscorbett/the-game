using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterMovement : MonoBehaviour

{
    // Start is called before the first frame update
    public Rigidbody rb;
    public float movespeed = 10f;
    public bool BallOnGround = true;

    private float xinput;
    private float zinput;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        // Jump if jump button is pressed and if ball is on the ground
        ProcessInputs();
        if (Input.GetButtonDown("Jump") && BallOnGround == true)
        {
            //How strong the jump is and says the ball is not on the floor anymore
            rb.AddForce(new Vector3(0, 9, 0), ForceMode.Impulse);
            BallOnGround = false;
        }

    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Floor")
        {
            BallOnGround = true;
        }
    }


    private void FixedUpdate()
    {
        Move();
    }

    private void ProcessInputs()
    {
        xinput = Input.GetAxis("Horizontal");
        zinput = Input.GetAxis("Vertical");
    }

    private void Move()
    {
        rb.AddForce(new Vector3(xinput, 0f, zinput) * movespeed);
    }
}
